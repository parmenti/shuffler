# -*- coding: utf-8 -*-
"""\
Author: Yannick Parmentier
Date: 2018/01/30
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys, functools
from lark     import Lark, Transformer, common, lexer
from loadXML  import Sem
from desclog  import DescLog

class FlatSemTransformer(Transformer):

    def formula(self, entries):
        return list(entries)

    def semlit(self, semlit):
        return ('semlit', semlit)

    def neg(self, neg):
        return ('neg', True)
        
    def label(self, label):
        return ('label', label[0])

    def pred(self, pred):
        return ('pred', pred[0])
    
    def args(self, args):
        return ('args', args)

    def value(self, v):
        return v[0]

    def var(self, var):
        return '?' + var[1]
    
    def name(self, name):
        return functools.reduce(lambda u,v : u+v, list(map(lambda x: x.value, name)))

    def string(self, s):
        return s[0].value[1:-1] #to remove \"\"

class FlatSemProcessor(object):

    def __init__(self):
        self._iast    = None     #transformed AST
        
        #parser spec for entries
        self._grammar = r"""
        formula : (semlit [";"])*

        semlit  : neg* label ":" pred "(" args ")"

        neg     : NEGATION
        label   : value
        pred    : value
        args    : (arg [","])*

        ?arg    : value
        
        value  : name
        | var
        | STRING -> string

        name    : (PLUS|MINUS|DOT|QUOTE|LETTER|DIGIT|NUMBER|UNDERSCORE)+
        var     : QUESTION name

        NEGATION: "!"
        QUESTION: "?"
        UNDERSCORE : "_"
        PLUS    : "+"
        MINUS   : "-"
        DOT     : "."
        QUOTE   : /[']/

        COMMENT : /^%[^\n]*/+
        | NEWLINE
        | WS

        NEWLINE : /\n/

        %import common.ESCAPED_STRING -> STRING
        %import common.SIGNED_NUMBER -> NUMBER
        %import common.LETTER
        %import common.DIGIT
        %import common.WS_INLINE -> WS
        %ignore COMMENT
        """
        self._parser = Lark(self._grammar, start='formula', parser='lalr', lexer='standard', transformer=FlatSemTransformer())

    @property
    def iast(self):
        return self._iast

    def parseFormula(self, flatsemformula, numSent, sentence): #sentence is kept for debugging purposes
        #alternative method to process a formula only
        self._iast = self._parser.parse(flatsemformula)
        semlits    = self._iast
        #print(semlits)  #(debug only)
        ## For debug only:
        # for e in semlits:
        #     print(str(e))
        #     for f in e[1]:
        #         print(f)
        #     print('**')
        dlformula  = self.getEntry(semlits)
        return DescLog(dlformula, numSent, sentence)

    def getEntry(self, fsentry):
        eentry = {}
        for fsformula in fsentry:
            efs  = dict(fsformula[1])
            eneg = False
            elab = efs['label'] #should be uniq!
            epred= efs['pred']
            eargs= efs['args']
            if 'neg' in efs.keys():
                eneg = True
            eentry[elab] = Sem(None, eneg, elab, epred, eargs) #None here is for the other use of the constructor
        return eentry


if __name__ == "__main__":
    if len(sys.argv) > 1:
        testfile = open(sys.argv[1], 'r')
        corpusIn = functools.reduce(lambda u, v: u + v, testfile.readlines())
        lines    = corpusIn.split('\n')
        nbentries= len(lines)
        fsp = FlatSemProcessor()
        for l in lines:
            if l == '':
                continue
            # print(l)
            # try:            
            dlformula = fsp.parseFormula(l) 
            print(dlformula)
            # except Exception as e:
            #     print('Ill-formed lexicon, please check your file', file=sys.stderr)
            #     print(e)

            
