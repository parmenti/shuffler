# -*- coding: utf-8 -*-
"""\ 
This module reads lexica encoded in XML format and returns a
Lexicon object composed of LexEntries. 
Author: Yannick Parmentier 
Date: 2017/11/21
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys
from lexicon  import LexEntry
from loadXML  import FS, feature_value
from lxml     import etree
from nltk.featstruct import FeatStruct, unify

class XMLlexloader(object):
    
    def __init__(self, filename, encoding):
        self.xmlfile = filename
        self.encoding= encoding

    def getLexicon(self, l, progload):
        parser    = etree.XMLParser(remove_comments=True, encoding=self.encoding)
        try:
            xmldoc   = etree.parse(self.xmlfile, parser = parser)
            nbentries= xmldoc.xpath('count(//lemma)') #len(xmldoc.xpath("/lexicon/lemmas/lemma"))
            if progload is not None:
                if 'printProgressBar' in dir(progload):
                    progload.setTotal(nbentries)
                else:
                    progload.max = nbentries
            for entry in xmldoc.xpath("/lexicon/lemmas/lemma"):
                lemma = self.getEntry(entry)
                if progload is not None:
                    progload.value += 1
                    if 'printProgressBar' in dir(progload):
                        progload.printProgressBar()
                l.addEntry(lemma)
        except etree.XMLSyntaxError:
            raise Exception("XML syntax error")

    def getEntry(self,xmlentry):
        elemma = ''   #str
        ecat   = ''   #str
        esem   = None #sem (see grammar module)
        efamily= ''   #str
        eacc   = ''   #str
        efilter= None #FS
        eexc   = ''   #FS
        eequa  = {}   #dict node-FS
        ecoanc = {}   #dict node-FS

        elemma = xmlentry.get('name').replace('+_','_')
        ecat   = xmlentry.get('cat')
        xmlsem = xmlentry.find('semantics', None)
        esem   = None if xmlsem is None else list(map(SemLex, xmlsem.findall('literal')))
        efamily= xmlentry.get('family')
        eacc   = xmlentry.get('acc')
        xmlfil = xmlentry.find('filter/fs', None)
        efilter= FeatStruct('[]') if xmlfil is None else FeatStruct(str(FS(xmlfil)))
        xmlfs  = xmlentry.find('interface/fs', None)
        eexc   = FeatStruct('[]') if xmlfs is None else FeatStruct(str(FS(xmlfs)))
        xmleq  = xmlentry.find('equations')
        if xmleq is not None:
            for eq in xmleq.findall('equation'):
                node   = eq.get('node_id')
                eqtype = eq.get('type') #bot
                eqfs   = eq.find('fs', None)
                if eqfs is not None:
                    eequa[node] = FeatStruct('[]')
                    eequa[node][eqtype] = FeatStruct(str(FS(eqfs)))
        xmlco  = xmlentry.find('coanchors')
        if xmlco is not None:
            for coa in xmlco.findall('coanchor'):
                node   = coa.get('node_id')
                coatype= coa.get('type') #top
                coafs  = coa.find('fs', None)
                if coafs is not None:
                    ecoanc[node] = FeatStruct('[]')
                    ecoanc[node][coatype] = FeatStruct(str(FS(coafs)))
        return LexEntry(elemma,ecat,esem,efamily,eacc,efilter,eexc,eequa,ecoanc)


class SemLex(object):

    def __init__(self, semlit):
        self.neg   = False
        self.label = None
        self.pred  = None
        self.args  = []
        if semlit.get("negated") is not None:
            self.neg = True
        lab = semlit.get("label")
        if lab is not None and lab != '_':
            self.label = lab
        pred = semlit.get("predicate")
        if pred is not None:
            self.pred = pred
        for arg in semlit.findall("args"):
            self.args.append(gensemlex(arg))        

    def __eq__(self,  other):
        return str(self) == str(other)

    def __repr__(self):
        s = ''
        if self.neg:
            s += "!"
        if self.label is not None:
            s += self.label + ':'
        if self.pred is not None:
            s += self.pred
            s += "("
        for a in self.args:
            s += a + ','
        if len(s) > 0 and s[len(s)-1] == ',':
            s = s[:-1]
            s+=')'
        return s

def gensemlex(i):
    s = ''
    if i.find('sym',None) is not None:
        if 'value' in i.find('sym').attrib:
            string= i.find('sym').get('value')
            if string in ['+','-']:
                s += '\''+value.get("value")+'\''
            elif '+_' in string:
                s += string.replace('+_','_')
            else:
                s += string
        elif 'varname' in i.find('sym').attrib: 
            s+= '?'+i.find('sym').get('varname')[1:]
    return s
