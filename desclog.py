# -*- coding: utf-8 -*-
"""\
This module contains the definition of Lexicon and LexEntries
objects. 
Author: Yannick Parmentier
Date: 2017/11/21
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import functools, sys

structs = {}
structs['exists_'] = 'ObjectSomeValuesFrom'
structs['forall_'] = 'ObjectAllValuesFrom'
structs['subset_'] = 'SubClassOf'
structs['conj_']   = 'ObjectIntersectionOf'
structs['and_']    = 'ObjectIntersectionOf'
structs['or_']     = 'ObjectUnionOf'
structs['not_']    = 'ObjectComplementOf'
structs['semtop_'] = 'owl:Thing'
suffixes           = ['Arg2_', 'Arg3_', 'Arg2inv_', 'Arg3inv_', 'Arg4_', 'object_']

def getPredAndSuffix(concept):
    conceptPred = concept
    conceptSuff = ''
    for suffix in suffixes:
        if concept.endswith(suffix):
            conceptPred = concept[:(len(concept)-len(suffix))]
            conceptSuff = concept[(len(concept)-len(suffix)):]
            break
    return (conceptPred, conceptSuff)
            
class DescLog(object):

    def __init__(self, entry, numSent, sentence): #entry is a dictionary of semlits
        self._sentence  = sentence
        self._id        = numSent
        self._formula   = None #(formula,rootLabels)
        self._dlform    = []   #output DL formula (list of DesLoglit)
        self._dlshapes  = []   #DL shapes (each one is a str)
        self._dlconcepts= []   #DL concepts (each one is a str)
        self._names     = {}   #dictionary for concept renaming (concept <-> int Id)
        #Finding out root labels (labels which are not arguments)
        arglabels = [] #list of labels appearing in args
        for e in entry.values():
            arglabels += e.args
        rootLabels= list(set(entry.keys()) - set(arglabels))
        self._formula = (entry, rootLabels)
        #Converting the FS formula into DL formula
        for lab in rootLabels:
            try:
                dlformula = self.convertFormula2DL(entry, lab)
                if len(rootLabels) > 1:
                    print('(1) Multi rooted flat semantic formula: ', file=sys.stderr)
                    print(self._id + ': ' + self._sentence, file=sys.stderr) 
                    print(self._formula, file=sys.stderr)
                    print(dlformula,     file=sys.stderr)
                elif dlformula.length() < len(entry):
                    print('(2) Uncomplete DL semantic formula: ', file=sys.stderr)
                    print(self._id + ': ' + self._sentence, file=sys.stderr)                     
                    print(self._formula, file=sys.stderr)
                    print(dlformula,     file=sys.stderr)
                else: #if there were no problem, we keep the DL formula (and associated shape + concept)
                    self._dlform.append(dlformula)
                    self._dlshapes.append(dlformula.getShape(self._names))
                    self._dlconcepts.extend(dlformula.getConcept())
            except RecursionError as e:
                print('(3) Cyclic flat semantic formula: ', file=sys.stderr)
                print(self._id + ': ' + self._sentence, file=sys.stderr)                 
                print(self._formula, file=sys.stderr)

    def printDL(self):
        return functools.reduce(lambda u, v: u + v, map(lambda x: str(x), self._dlform)) if len(self._dlform) > 0 else ''

    def printShapes(self):
        return functools.reduce(lambda u, v: u + v, map(lambda x: str(x), self._dlshapes)) if len(self._dlshapes) > 0 else ''

    def printConcepts(self):
        return functools.reduce(lambda u, v: u + v, map(lambda x: str(x) + ' ', self._dlconcepts)) if len(self._dlconcepts) > 0 else ''

    def __repr__(self):
        debug = False
        s = ''
        f,rootlabs = self._formula 
        desclogf   = self._dlform
        #f is a dict (label,semlits)
        fsformula = functools.reduce(lambda u, v: u + v, map(lambda x : str(x), f.values())) 
        dlformula = functools.reduce(lambda u, v: u + v, map(lambda x: str(x), desclogf)) if len(desclogf) > 0 else ''
        if debug:
            s += fsformula + ' ' + str(rootlabs) + '\n' 
        s += dlformula
        if debug:
            s += '\n\n' #debug only
        return s

    def convertFormula2DL(self, formula, nextLabel):
        #recursive traversal of the Flat Sem formula        
        semlit = formula[nextLabel]
        pred   = semlit.pred if semlit.pred not in structs.keys() else structs[semlit.pred]
        dlform = DescLoglit(pred)
        for a in semlit.args:
            if a in formula.keys(): #recursive call
                dlform.addArg(self.convertFormula2DL(formula,a))
            elif a[-1] == '_': #structural individual
                apred = a if a not in structs.keys() else structs[a]
                dlform.addArg(DescLoglit(apred))
        return dlform
        
class DescLoglit(object):
    
    def __init__(self, fspred):
        self.concept = fspred
        self.args    = []

    def addArg(self, fsarg):
        self.args.append(fsarg)
        
    def __eq__(self,  other):
        return str(self) == str(other)

    def length(self):
        return 1 + sum(map(lambda x : x.length(), self.args))
    
    def getShape(self, dictnames):
        shape      = ''
        conceptVar = self.concept #default value
        #First, concept anonymization
        if self.concept not in structs.values():
            if self.concept[-1] != '_':
                #conceptual predicate
                if self.concept in dictnames.keys():
                    conceptVar = ':C' + str(dictnames[self.concept])
                else:
                    currentKey = sorted(dictnames.values())[-1] if len(dictnames.values()) > 0 else 1
                    conceptVar = ':C' + str(currentKey + 1)
                    dictnames[self.concept] = (currentKey + 1)
            else:
                #structural predicate
                #print(self.concept) #debug
                conceptPred, conceptSuff = getPredAndSuffix(self.concept)
                if conceptPred in dictnames.keys():
                    conceptVar = ':XC' + str(dictnames[conceptPred]) + ' ' + conceptSuff
                else:
                    currentKey = sorted(dictnames.values())[-1] if len(dictnames.values()) > 0 else 1
                    conceptVar = ':XC' + str(currentKey + 1) + ' ' + conceptSuff
                    dictnames[conceptPred] = (currentKey + 1)
        #Second, recursive shape building
        shape += conceptVar
        if len(self.args) > 0:
            shape += ' ( '
            for a in self.args:
                shape += a.getShape(dictnames) + ' '
            shape += ') '
        return shape

    def getConcept(self):
        concepts = []
        if self.concept not in structs.values():
            if self.concept[-1] != '_':
                #conceptual predicate
                concepts.append(self.concept)
            else:
                #structural predicate
                conceptPred,_ = getPredAndSuffix(self.concept)
                concepts.append(conceptPred)
        for a in self.args:
            concepts.extend(a.getConcept())
        return concepts

    def __repr__(self):
        s = ''
        if self.concept is not None:
            conceptPred, conceptSuff = getPredAndSuffix(self.concept)
            s += conceptPred + ' '
            if conceptSuff != '':
                s += conceptSuff + ' '
            if len(self.args) > 0:
                s += '( '
                for a in self.args:
                    s += str(a) + ' '
                s+=') '
        return s
