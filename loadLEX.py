# -*- coding: utf-8 -*-
"""\
Author: Yannick Parmentier
Date: 2017/12/01
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys, functools
from lark     import Lark, Transformer, common, lexer
from lexicon  import LexEntry, SemLex
from nltk.featstruct import FeatStruct, unify

class LexTransformer(Transformer):

    def lexicon(self, entries):
        return list(entries)

    def entry(self, entry):
        return ('entry', entry)

    def lemma(self, lemma):
        return ('lemma', lemma[0])

    def field(self, field):
        return field[0]

    def cat(self, cat):
        return ('cat', cat[0])

    def sem(self, sem):
        if len(sem) > 0:
            return ('sem', (sem[0], sem[1]))
        else:
            return ('sem', None)

    def fam(self, fam):
        return ('fam', fam[0])
    
    def filters(self, filters):
        return ('filters', filters[0])

    def exc(self, exc):
        return ('exc', exc)

    def equations(self, eq):
        return ('equations', eq)

    def equ(self, e):
        return (e[0],e[1],e[2])

    def path(self, p):
        return p

    def pathname(self, pn):
        return pn[1]

    def coanchors(self, coanc):
        return ('coanchors', coanc)

    def coanc(self, c):
        return (c[0],c[1],c[2])
    
    def avm(self, avm):
        return avm

    def pair(self, p):
        return (p[0], p[1])

    def value(self, v):
        return v[0]
    
    def xname(self, name):
        return functools.reduce(lambda u,v : u+v, list(map(lambda x: x.value, name)))

    def name(self, name):
        return functools.reduce(lambda u,v : u+v, list(map(lambda x: x.value, name)))

    def string(self, s):
        return s[0].value[1:-1] #to remove \"\"

    def number(self, n):
        return ('acc', float(n[0].value))

    
class LexProcessor(object):

    def __init__(self):
        self._iast    = None     #transformed AST
        
        #parser spec for entries
        self._grammar = r"""
        lexicon : (entry)*

        entry : lemma (field)* 

        lemma : "*ENTRY:" name 

        field : cat
        | sem
        | acc
        | fam
        | filters
        | exc
        | equations
        | coanchors

        cat : "*CAT:" name
        sem : "*SEM:" (name avm)*
        acc : "*ACC:" NUMBER -> number
        fam : "*FAM:" name 
        filters : "*FILTERS:" avm 
        exc : "*EX:" "{" [pair ("," pair)*] "}" 
        equations : "*EQUATIONS:" (equ [","])*
        coanchors : "*COANCHORS:" (coanc [","])*

        equ  : name "->" path "=" value
        coanc: name "->" name "/" value
        
        path : xname (pname)*

        xname : (UNDERSCORE|PLUS|MINUS|QUOTE|LETTER|DIGIT|NUMBER)+
        pname : "." xname -> pathname

        value : avm
        | name
        | STRING -> string

        avm : "[" [pair ("," pair)*] "]"
        pair : name "=" value

        name : (UNDERSCORE|PLUS|MINUS|DOT|QUOTE|LETTER|DIGIT|NUMBER)+

        UNDERSCORE : "_"
        PLUS : "+"
        MINUS: "-"
        DOT :"."
        QUOTE : /[']/

        COMMENT : /^%[^\n]*/+
        | NEWLINE
        | WS

        NEWLINE : /\n/

        %import common.ESCAPED_STRING -> STRING
        %import common.SIGNED_NUMBER -> NUMBER
        %import common.LETTER
        %import common.DIGIT
        %import common.WS_INLINE -> WS
        %ignore COMMENT
        """
        self._parser = Lark(self._grammar, start='lexicon', parser='lalr', lexer='standard', transformer=LexTransformer())

    @property
    def iast(self):
        return self._iast

    def parseLex(self, lexicon, filecontent, progload):
        self._iast= self._parser.parse(filecontent) 
        #print(self._iast)#print Abstract Syntax Tree (debug only)
        #new_tree = LexTransformer().transform(self._iast)
        new_tree = self._iast
        #print(new_tree)  #print Interpreted Abstract Syntax Tree (debug only)
        ## For debug only:
        #for e in new_tree:
        #    print(dict((x,y) for x, y in e[1]))
        if lexicon is not None:
            nbentries = len(new_tree)
            if progload is not None:
                if 'printProgressBar' in dir(progload):
                    progload.setTotal(nbentries)
                else:
                    progload.max = nbentries    
            for i in new_tree:
                lemma = self.getEntry(dict((x,y) for x, y in i[1]))
                if progload is not None:
                    progload.value += 1
                    if 'printProgressBar' in dir(progload):
                        progload.printProgressBar()
                lexicon.addEntry(lemma)

    def getEntry(self, dentry):
        elemma = dentry['lemma']   #str
        ecat   = dentry['cat']     #str
        esem   = SemLex(dentry['sem'])
        efamily= dentry['fam']     #str
        eacc   = dentry['acc']     #str
        efilter= None              #FS
        eexc   = dentry['exc']     #list
        eequa  = {}   #dict node-FS
        ecoanc = {}   #dict node-FS
        fil    = dict((x,y) for x, y in dentry['filters'])
        ffs    = '['
        for ffeatval in fil:
            realval = ffeatval[1]
            if realval in ['+','-']:
                realval = '\'' + realval + '\''
            ffs += ffeatval[0] + '=' + realval + ','
        ffs   += ']'
        efilter = FeatStruct(ffs)
        for node, path, feat in dentry['equations']:
            val = feat
            if val in ['+','-']:
                val = '\''+val+'\''
            fs = ''
            i  = 0
            for p in path:
                fs += '[' + p + '='
                i += 1
            fs += val
            for j in range(i,0,-1):
                fs += ']'
            eequa[node] = FeatStruct(fs)
        for node,lex,_ in dentry['coanchors']:
            ecoanc[node] = FeatStruct('[' + 'cat=\'' + lex + '\']')
        return LexEntry(elemma, ecat, esem, efamily, eacc, efilter, eexc, eequa, ecoanc)
        

if __name__ == "__main__":
    if len(sys.argv) > 1:
        testfile = open(sys.argv[1], 'r')
        #lex = functools.reduce(lambda u, v: u + v, list(map(lambda x : x.translate({ord(c): None for c in '\''}), testfile.readlines())))
        lex = functools.reduce(lambda u, v: u + v, testfile.readlines())
        #print(lex)
        p = LexProcessor()
        # try:
        p.parseLex(None, lex, None)
        iast = p.iast
        # except Exception as e:
        #     print('Ill-formed lexicon, please check your file', file=sys.stderr)
        #     print(e)
            
