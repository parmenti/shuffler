# -*- coding: utf-8 -*-
"""\ 
This module reads tree grammars encoded in XML format (see DTD at
https://sourcesup.renater.fr/xmg/xmg-tag.dtd,xml) and returns a
TAGrammar object composed of TAGEntries.  It reuses extracts of code
from XMG2's treeview by Jean-Baptiste Perrin and Denys Duchier.
Author: Yannick Parmentier 
Date: 2017/01/23
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys
from grammar         import TAGEntry
from lxml            import etree
from nltk.featstruct import FeatStruct, unify
from nltk.sem.logic  import Variable

class XMLloader(object):
    
    def __init__(self, filename, encoding):
        self.xmlfile = filename
        self.encoding= encoding

    def getGrammar(self, g, progload):
        parser    = etree.XMLParser(remove_comments=True, encoding=self.encoding)
        try:
            xmldoc   = etree.parse(self.xmlfile, parser = parser)
            nbentries= xmldoc.xpath('count(//entry)') #len(xmldoc.xpath("/grammar/entry"))
            if progload is not None:
                if 'printProgressBar' in dir(progload):
                    progload.setTotal(nbentries)
                else:
                    progload.max = nbentries
            for entry in xmldoc.xpath("/grammar/entry"):
                tage = self.getEntry(entry)
                if progload is not None:
                    progload.value += 1
                    if 'printProgressBar' in dir(progload):
                        progload.printProgressBar()
                g.addEntry(tage)
        except etree.XMLSyntaxError:
            raise Exception("XML syntax error")

    def getEntry(self,xmlentry):
        ttype      = self.getType(xmlentry)
        tsubst     = {} #dict cat-nodes
        tadj       = {} #dict cat-nodes
        tcoanc     = {} #dict name-node
        tnames     = {} #dict name-node        
        tanc       = [] #nodes
        tfoot      = [] #nodes        
        tname      = xmlentry.get('name').replace('-','_') #cf. dash forbidden in feature names (FeatStruct restriction)
        tfamily    = xmlentry.find('family').text
        ttrace     = []
        for classe in xmlentry.find('trace').iter('class'):
            ttrace.append(classe.text)
        xmlsem     = xmlentry.find('semantics', None)
        tsemantics = None if xmlsem is None else list(map(Sem, xmlsem.findall('literal')))
        xmlfs      = xmlentry.find('interface/fs', None)
        tiface     = FeatStruct('[]') if xmlfs is None else FeatStruct(str(FS(xmlfs)))
        ttree      = Node(xmlentry.find('tree/node'), None, tfamily, 'e', 0, tsubst, tadj, tcoanc, tnames, tanc, tfoot) #'e' is the root gorn address
        return TAGEntry(ttype,tsubst,tadj,tcoanc,tnames,tanc,tfoot,tname,tfamily,ttrace,ttree,tsemantics,tiface)


    def getType(self, xmlentry):
        ttype = None
        rootcat = xmlentry.find("tree/node/narg/fs/f[@name='cat']/sym").get('value',None) 
        #print(rootcat)
        mandadj = False
        if rootcat == 's':
            topmod = xmlentry.find("tree/node/narg/fs/f[@name='top']/fs/f[@name='modified']/sym")
            botmod = xmlentry.find("tree/node/narg/fs/f[@name='bot']/fs/f[@name='modified']/sym")
            if topmod is not None and botmod is not None and topmod.get('value',None) != botmod.get('value',None):
                mandadj = True
                #print(mandadj)
        footcat = None
        footnode= xmlentry.find("tree//node[@type='foot']/narg/fs/f[@name='cat']/sym")
        if footnode is not None:
            footcat = footnode.get('value',None) 
            #print(footcat)
            ttype = footcat.upper() + 'foot'
        else:
            if rootcat == 's':
                if mandadj:
                    ttype = 'Srootmadj'
                else:
                    ttype = 'Sroot'
            else:
                ttype = rootcat.upper() + 'root'
        return ttype
        
        
class Node(object):

    def __init__(self, xmlnode, mother, family, gorn, rank, tsubst, tadj, tcoanc, tnames, tanc, tfoot):
        self._color   = 'black' #for tree diff        
        self._ntype   = xmlnode.get("type",None) if xmlnode is not None else 'std'
        self._name    = xmlnode.get("name",None) if xmlnode is not None else ''
        if xmlnode is not None and xmlnode.get("name",None) is not None: #we keep track of named nodes (for anchoring equations)
            tnames[self._name] = [self] #list for easier update at adjunction
        self._address = gorn
        self._mother  = mother
        self._rank    = rank
        xmlfs = xmlnode.find("narg/fs", None) if xmlnode is not None else None
        self._fs      = FeatStruct('[]') if xmlfs is None else FeatStruct(str(FS(xmlfs)))
        #we add top and bot features if they do not yet exist
        if not self._fs.has_key('top'):
            self._fs['top'] = FeatStruct('[]')
        if not self._fs.has_key('bot'):
            self._fs['bot'] = FeatStruct('[]')    
        #we index specific nodes
        if self._ntype == 'subst':
            if self.cat() in tsubst.keys():
                tsubst[self.cat()].append(self)
            else:
                tsubst[self.cat()] = [self]
        elif self._ntype == 'anchor':
            self._fs['fam'] = family #project family into anchor
            tanc.append(self) #anchor is unique within a tree but will be accumulated (hence list)
        elif self._ntype == 'coanchor':
            tcoanc[self._name] = [self] #list for easier update at adjunction
        elif self._ntype in ['lex','flex']:
            pass
        elif self._ntype == 'nadj':
            pass
        elif self._ntype == 'foot':
            tfoot.append(self)
        else: #regular inner node (cf adj), type==std
            if self.cat() in tadj.keys():
                tadj[self.cat()].append(self)
            else:
                tadj[self.cat()] = [self]
        self._children = tuple([Node(x, self, family, gorn + '.' + str(i), i, tsubst, tadj, tcoanc, tnames, tanc, tfoot) for i,x in enumerate(xmlnode.findall("node"))]) if xmlnode is not None else []
        
    def cat(self):
        nodeCat = ''
        if self._fs != None and self._fs.has_key('cat'):
            nodeCat = str(self._fs['cat'])
        # else:
        #     sys.stderr.write('Unknown feature \'cat\' in FS: ' + str(self._fs))
        #     sys.stderr.flush()
        return nodeCat

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, value):
        self._children = value

    @property
    def ntype(self):
        return self._ntype

    @ntype.setter
    def ntype(self, value):
        self._ntype = value
        
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

    @property
    def address(self):
        return self._address

    @property
    def mother(self):
        return self._mother

    @property
    def rank(self):
        return self._rank

    @property
    def fs(self):
        return self._fs

    @fs.setter
    def fs(self, value):
        self._fs = value
        
    def childCount(self):
        return len(self._children)

    def child(self, i):
        try:
            return self._children[i]
        except IndexError:
            raise Exception("Unknown node")
    
    def __eq__(self, other):
        if self._ntype != other._ntype:
            return False
        else:
            return self._fs == other._fs

    def __repr__(self):
        repr_children = ",".join([repr(e) for e in self._children])
        name = self._name if self._name is not None else self.cat()
        return "%s<%s>" % (name, repr_children)

    def prettyprint(self):
        repr_children = ",".join([e.prettyprint() for e in self._children])
        if len(repr_children) > 0:
            # To print node memory address (for debug):
            #return "%s[%s](%s)" % (self.cat(), id(self), repr_children)
            return "%s(%s)" % (self.cat(), repr_children)            
        else:
            # To print node memory address (for debug):            
            #return "%s[%s]" % (self.cat(), id(self))
            return "%s" % self.cat()

        
class FS(object): #became utility type
                  #(to get the FS str representation and pass it to NLTK's FeatStruct)

    def __init__(self, xmlfs):
        self.coref = xmlfs.get("coref", None)
        self.table = {}
        for xmlf in xmlfs.findall("f"):
            if xmlf.find('*', None) is None:
                self.table[xmlf.get("name")] = '?'+xmlf.get("varname")[1:]
            else:
                self.table[xmlf.get("name")] = feature_value(xmlf.find("*"))

    def prettyprint(self, avm, indent=0):
        s = ''
        for k in avm.table.keys():
            if k != 'cat': #cat already processed
                s += '  ' + '    ' * indent + k + ':' #always 2 leading ' '
                if isinstance(avm.table[k], str):
                    s += avm.table[k] + ';'
                elif isinstance(avm.table[k], ALT):
                    s += str(avm.table[k]) + ';'
                elif isinstance(avm.table[k], FS):
                    s += ';' + self.prettyprint(avm.table[k], indent+1) + ';' 
                else: #should never happen (cf dtd)
                    print('Unknown feature type: ' + str(avm.table[k]))
        return s

    def __eq__(self, other):
        if other != None:
            return self.table['cat'] == other.table['cat']
        else:
            return False
        
    def __repr__(self):
        s = '['
        for x in self.table.keys():
            s += x + '=' + str(self.table[x]) + ','
        s +=']'
        return s
    
def feature_value(value):
    tag = value.tag
    assert tag in ("fs","sym","vAlt")
    if value.tag == "fs":
        return FS(value)    
    if value.tag == "sym":
        if not value.get('varname') is None:
            return '?'+value.get('varname')[1:]
        else:
            if value.get("value") in ['+','-']:
                return '\''+value.get("value")+'\''
            elif '+_' in value.get("value"):
                return value.get("value").replace('+_','_')
            else:
                return value.get("value")
    if value.tag == "vAlt":
        return ALT(value).values

class ALT(object):

    def __init__(self, alt):
        self.coref = alt.get("coref", None)
        self.values = tuple(map(feature_value,alt.findall("sym")))

    def __eq__(self,  other):
        if str(other).find('ALT') > -1:
            s = ''
            for i in self.values:
                s += i + '|'
            y = ''
            for i in other.values:
                y += i + '|'
            return s == y
        else:
            return False

    def __repr__(self):
        s = '{'
        first = True
        for x in self.values:
            if not(first):
                s += '|' + x
            else:
                s += x
                first = False
        s +='}'
        return s

class Sem(object):

    def __init__(self, semlit = None, eneg = None, elabel = None, epred = None, eargs = None):
        if eneg is None and elabel is None and epred is None and eargs is None:
            self.neg   = False
            self.label = None
            self.pred  = None
            self.args  = []
            if semlit is not None:
                if semlit.get("negated") != 'no':
                    self.neg = True
                lab = semlit.find("label", None)
                if lab is not None:
                    self.label = gensem(lab)
                pred = semlit.find("predicate")
                if pred is not None:
                    self.pred = gensem(pred)
                for arg in semlit.findall("arg"):
                    self.args.append(gensem(arg))        
        else:
            self.neg   = eneg
            self.label = elabel
            self.pred  = epred
            self.args  = eargs
                       
    def __eq__(self,  other):
        return str(self) == str(other)

    def __repr__(self):
        s = ''
        if self.neg:
            s += '!'
        if self.label is not None:
            s += self.label + ' : '
        if self.pred is not None:
            s += self.pred
            s += ' ( '
        for a in self.args:
            s += a + ' , '
        if len(s) > 0 and s[len(s)-2] == ',': #-2 cause there is a final ' '
            s = s[:-2]
            s+=') '
        return s

    def substitute(self, semlex):
        mapping = {}
        for k,v in semlex.items():
            key, value = k,v
            if isinstance(k, Variable):
                key = str(k)
            if isinstance(v, Variable):
                value = str(v)
            mapping[key] = value
        if self.label in mapping:
            self.label = mapping[self.label]
        if self.pred in mapping:
            self.pred = mapping[self.pred]
        for iarg in range(len(self.args)):
            if self.args[iarg] in mapping:
                self.args[iarg] = mapping[self.args[iarg]]

    def rename(self, nameGen):
        ## function used to rename *variables* with simpler names
        self.label= self.getName(self.label, nameGen)
        self.pred = self.getName(self.pred, nameGen)
        for i,_ in enumerate(self.args):
            self.args[i] = self.getName(self.args[i], nameGen)
        
    def getName(self, var, nameGen):
        #print('Looking up: ' + var)
        if var[0] == '?':
            res = nameGen.getName(var)
            #print('=> ' + res)
            return '?' + res
        else:
            #print('=> ' + var)
            return var

    def getConcepts(self):
        concepts = []
        concepts.append(self.getConceptName(self.pred))
        for i in self.args:
            concepts.append(self.getConceptName(i))
        return [x for x in concepts if x is not None]

    def getConceptName(self, var):
        if var.startswith('?'):
            return None
        else:
            if var.endswith('_'): #convention used to name structural constants (and_, or_, etc.)
                lastUpperCasePos = var.rfind('A')
                if lastUpperCasePos == -1:
                    return None
                else:
                    return var[:lastUpperCasePos]
            else:
                return var

    def getGenericName(self):
        newSemlit = Sem()
        ## function used to get generic names (keeping only structural constants if any)
        newSemlit.label= self.getGenericConstName(self.label)
        newSemlit.pred = self.getGenericConstName(self.pred)
        newSemlit.args = []
        for i,_ in enumerate(self.args):
            newSemlit.args.append(self.getGenericConstName(self.args[i]))
        return newSemlit

    def getGenericConstName(self, var):
        if var.startswith('?'):
            return var
        else:
            if var.endswith('_'): #convention used to name structural constants (and_, or_, etc.)
                lastUpperCasePos = var.rfind('A')
                if lastUpperCasePos == -1:
                    return var
                else:
                    return var[lastUpperCasePos:]
            else:
                return '_'
                
def gensem(i):
    s = ''
    if 'value' in i.find('sym').attrib:
        s+= i.find('sym').get('value')
    elif 'varname' in i.find('sym').attrib: 
        s+= '?'+i.find('sym').get('varname')[1:]
    return s
