# -*- coding: utf-8 -*-
"""\
This module contains the definition of Lexicon and LexEntries
objects. 
Author: Yannick Parmentier
Date: 2017/11/21
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pickle, sys, json
from   nltk.featstruct import FeatStruct
from   nltk.sem.logic  import Variable
from   utilities       import nextint
from   functools       import reduce

class Lexicon(object):

    def __init__(self):
        self._entries = {} #dict lemma-entries
        self._families= {} #dict family-entries
        self._size    =  0 #grammar size (for e.g. progress bars)

    @property
    def entries(self):
        return self._entries
    
    @property
    def families(self):
        return self._families

    @property
    def size(self):
        return self._size

    def addEntry(self, entry):
        if entry.family in self._families.keys():
            self._families[entry.family].append(entry)
        else:
            self._families[entry.family] = [entry]
        if entry.lemma in self._entries.keys():
            self._entries[entry.lemma].append(entry)
        else:
            self._entries[entry.lemma] = [entry]

    def getEntries(self, lemma):
        return self._entries[lemma]
                    
    def setSize(self): #used to initialize progress bars
        self._size = reduce(lambda x, y: x + y, list(map(lambda z: len(self._entries[z]), self._entries.keys())), 0)
        
    def save(self, filename):
        output = open(filename, 'wb')
        pickle.dump((self._entries, self._families), output)

    def load(self, filename):
        pkl_file = open(filename, 'rb')
        self._entries, self._families = pickle.load(pkl_file)

    def __repr__(self):
        res = ''
        for lexe in self._entries.values():
            #res += '\n' + str(lexe) + ':'
            for item in lexe:
                res += str(item)
            res += '\n'
        return res

        
class LexEntry(object):
    
    def __init__(self, elemma, ecat, esem, efamily, eacc, efilter, eexc, eequa, ecoanc):
        self.__lemma     = elemma      #str
        self.__cat       = ecat        #str
        self.__sem       = esem        #sem (see grammar module)        
        self.__family    = efamily     #str
        self.__acc       = eacc        #str
        self.__filter    = efilter     #FS   (see loadXML module)
        self.__exc       = eexc        #FS
        self.__equations = eequa       #dict node-FS
        self.__coanchors = ecoanc      #dict node-FS

    @property
    def lemma(self):
        return self.__lemma

    @property
    def family(self):
        return self.__family

    @property
    def sem(self):
        return self.__sem

    @property
    def filter(self):
        return self.__filter

    @property
    def exc(self):
        return self.__exc

    @property
    def equations(self):
        return self.__equations

    @property
    def coanchors(self):
        return self.__coanchors

    ##setters
    @lemma.setter
    def lemma(self, value):
        self.__lemma = value
        
    @family.setter
    def family(self, value):
        self.__family = value

    @sem.setter
    def sem(self, value):
        self.__sem = value

    @filter.setter
    def filter(self, value):
        self.__filter = value

    @exc.setter
    def exc(self, value):
        self.__exc = value

    @equations.setter
    def equations(self, value):
        self.__equations = value

    @coanchors.setter
    def coanchors(self, value):
        self.__coanchors = value

        
    def __repr__(self):
        return '\n\n*ENTRY: ' + self.__lemma + '\n*CAT: ' + self.__cat + '\n*SEM: ' + str(self.__sem) + '\n*ACC: ' + str(self.__acc) + '\n*FAM: ' + self.__family + '\n*FILTERS: ' + str(self.__filter) + '\n*EX: ' + str(self.__exc) + '\n*EQUATIONS: ' + str(self.__equations) + '\n*COANCHORS: ' + str(self.__coanchors) + '\n'

class SemLex(object):

    def __init__(self, semlit):
        self.className = semlit[0] if semlit is not None else None
        self.params    = dict((x,y) for x, y in semlit[1]) if semlit is not None else dict()
        
    def __eq__(self,  other):
        return str(self) == str(other)

    def __repr__(self):
        s = ''
        if self.className is not None:
            s+= self.className
        s += '['
        for k in sorted(self.params):
            v = self.params[k]
            s += k + ':' + v + ','
        if len(s) > 0 and s[len(s)-1] == ',':
            s = s[:-1]
        s+=']'
        return s

