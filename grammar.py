# -*- coding: utf-8 -*-
"""\
This module contains the definition of TAGrammar and TAGEntries
objects. It provides various facilities such as tree annotation.
Author: Yannick Parmentier
Date: 2017/10/16
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pickle, sys, json
from   lxml            import etree
from   copy            import deepcopy
from   nltk.featstruct import FeatStruct
from   nltk.sem.logic  import Variable
from   utilities       import nextint

class TAGrammar(object):

    def __init__(self):
        self._entries = {} #dict name-entry
        self._families= [] #list of str (for display)
        self._indexed = {} #dict family-entries
        self._size    =  0 #grammar size (for e.g. progress bars)
        ## For shuffling (tree rewriting)
        self._shuffles= {} #dict label(e.g. Sroot, Srootmadj, AUXfoot,etc)-entries

    @property
    def entries(self):
        return self._entries
    
    @property
    def families(self):
        return self._families

    @property
    def indexed(self):
        return self._indexed

    @property
    def shuffles(self):
        return self._shuffles

    @property
    def size(self):
        return self._size

    def addEntry(self, entry):
        self._entries[entry.name] = entry #entry name must be unique
        if entry.family in self._indexed.keys():
            self._indexed[entry.family].append(entry)
        else:
            self._indexed[entry.family] = [entry]
        if entry.type is not None:
            if entry.type in self._shuffles.keys():
                self._shuffles[entry.type].append(entry)
            else:
                self._shuffles[entry.type] = [entry]

    def getentry(self, name):
        return self._entries[name]
                    
    def setSize(self): #used to initialize progress bars
        self._size = len(self._entries.keys())
    
    def grammar2json(self, filename, progload):
        res = dict()
        res['file']    = filename
        res['entries'] = list()
        for e in self._entries.keys():
            entry = self._entries[e]
            if progload is not None:
                progload.value += 1
                if 'printProgressBar' in dir(progload):
                    progload.printProgressBar()
            #print('Processing ' + str(entry)) #debug only
            res['entries'].append(entry2json(entry))
        with open(filename, 'w') as outfile:
            json.dump(res, outfile, indent=4)

    def save(self, filename):
        output = open(filename, 'wb')
        pickle.dump((self._entries, self._families, self._indexed), output)

    def load(self, filename):
        pkl_file = open(filename, 'rb')
        self._entries, self._families, self._indexed = pickle.load(pkl_file)

    def __repr__(self):
        res = ''
        for tree in self._entries.values():
            res += '\n' + str(tree)
        res += '\n\n****************************\n\n'
        for treetype in sorted(self._shuffles.keys()):
            res += '\t{:10} : {: >4}\n'.format(treetype, str(len(self._shuffles[treetype])))
        res += '\n****************************\n'  
        return res

        
class TAGEntry(object):
    
    def __init__(self, ttype, tsubst, tadj, tcoanc, tnames, tanc, tfoot, tname, tfamily, ttrace, ttree, tsemantics, tiface):
        """
        Return a TAG entry made of a name, a trace, a syntactic tree, a semantic representation and an syn/sem interface
        """
        self.__type      = ttype       #str
        self.__subst     = tsubst      #dict cat-nodes
        self.__adj       = tadj        #dict cat-nodes
        self.__coanc     = tcoanc      #dict name-node
        self.__names     = tnames      #dict name-node (for equations)
        self.__anc       = tanc        #nodes
        self.__foot      = tfoot       #nodes        
        self.__name      = tname       #str
        self.__family    = tfamily     #str
        self.__trace     = ttrace      #list of str
        self.__tree      = ttree       #Node (see loadXML module)
        self.__semantics = tsemantics  #str
        self.__iface     = tiface      #FS   (see loadXML module)
        self.__isInitial = True        #by default it is an initial (i.e. not derived) tree
        self.__adjoined  = {}          #number of adjoined trees per category (to control tree rewriting)
        self.__bindings  = None        #to store the bindings for further unifications

    @property
    def isInitial(self):
        return self.__isInitial

    @property
    def bindings(self):
        return self.__bindings

    @property
    def type(self):
        return self.__type

    @property
    def subst(self):
        return self.__subst

    @property
    def adj(self):
        return self.__adj

    @property
    def coanc(self):
        return self.__coanc

    @property
    def names(self):
        return self.__names

    @property
    def anc(self):
        return self.__anc

    @property
    def foot(self):
        return self.__foot

    @property
    def name(self):
        return self.__name

    @property
    def family(self):
        return self.__family

    @property
    def trace(self):
        return self.__trace

    @property
    def tree(self):
        return self.__tree

    @property
    def semantics(self):
        return self.__semantics

    @property
    def iface(self):
        return self.__iface

    @property
    def adjoined(self):
        return self.__adjoined

    ##setters
    @isInitial.setter
    def isInitial(self, value):
        self.__isInitial = value

    @bindings.setter
    def bindings(self, value):
        self.__bindings = value

    @name.setter
    def name(self, value):
        self.__name = value
        
    @anc.setter
    def anc(self, value):
        self.__anc = value
        
    @foot.setter
    def foot(self, value):
        self.__foot = value

    @trace.setter
    def trace(self, value):
        self.__trace = value

    @semantics.setter
    def semantics(self, value):
        self.__semantics = value

    @iface.setter
    def iface(self, value):
        self.__iface = value

    @adjoined.setter
    def adjoined(self, value):
        self.__adjoined = value

    def updateAdjoined(self, other):
        #other must be a dictionary cat<->int !
        for (k,v) in other.items():
            if k in self.__adjoined:
                self.__adjoined[k] += v
            else:
                self.__adjoined[k]  = v

    def getSignature(self):
        sig = '{'
        for k in sorted(self.__adjoined.keys()):
            sig += str(k) + ':' + str(self.__adjoined[k]) + '-'
        sig += '}'
        return sig
    
    def __repr__(self):
        return '\n***********************\nEntry: ' + self.__name + '\nFamily: ' + self.__family + '\nTrace: ' + ' '.join(self.__trace) + '\nTree: ' + str(self.__tree) + '\n(height:' + str(self.treeheight(self.__tree)) + ', width:' +  str(self.nbleaves(self.__tree)) + ')\n' + 'Semantics:\n' + str(self.__semantics) + '\nIface:\n' + str(self.__iface)


    def getprojection(self, node, proj):
        '''computes projection (ordered list of leaves)'''
        nbchild = node.childCount()
        if nbchild == 0:
            proj.append(node.cat())
        else:
            for i in range(nbchild):
                self.getprojection(node.child(i), proj)
    
    
    def treeheight(self, node):
        '''computes (sub)tree height'''
        nbchild = node.childCount()
        if nbchild > 0:
            listH=[]
            for i in range(nbchild):
                listH.append(self.treeheight(node.child(i)))
            return 1 + max(listH)
        else:
            return 1

        
    def nbleaves(self, node):
        '''computes total number of leave nodes'''
        nbchild = node.childCount()
        if nbchild == 0:
            return 1
        else:
            nb = 0
            for i in range(nbchild):
                nb += self.nbleaves(node.child(i))
            return nb

        
    def updateVars(self, counter):
        '''rename all variables with uniq names made of tree id + global counter'''
        newPrefix = self.__name + '_' + str(counter) + '_'
        self.__iface = FeatStruct(self.updateFs(newPrefix,   self.__iface, 0, False))
        self.updateSem(newPrefix,  self.__semantics)
        self.updateNode(newPrefix, self.__tree)

        
    def updateFs(self, prefix, fs, mode, isAnchor):
        #mode:0 => iface (renaming of feature names and values)
        #mode:1 => node fs (renaming of feature values only)
        #isAnchor:True => add "prefix" feature in fs
        newfs = '['
        for k,v in fs.items():
            if mode == 0:
                if isinstance(v,Variable):
                    newfs += prefix + k + '=' + '?' + prefix + str(v)[1:] +','
                elif isinstance(v,FeatStruct):
                    newfs += prefix + k + '=' + self.updateFs(prefix, v, mode, False) +','
                else:
                    newfs += prefix + k + '=\'' + str(v) +'\','
            elif mode == 1:
                if isinstance(v,Variable):                
                    newfs += k + '=' + '?' + prefix + str(v)[1:] +','
                elif isinstance(v,FeatStruct):
                    newfs += k + '=' + self.updateFs(prefix, v, mode, False) +','
                else:
                    newfs += k + '=\'' + str(v) +'\','
        if isAnchor: #prefix only goes to the top-level of the FS of the anchor
            newfs += 'prefix = \'' + prefix +'\','
        newfs += ']'
        return newfs
                
    def updateSem(self, prefix, sem):
        for semlit in sem:
            if semlit.label is not None and semlit.label[0] == '?':
                semlit.label = '?' + prefix + semlit.label[1:]
            if semlit.pred is not None and semlit.pred[0] == '?':
                semlit.pred = '?' + prefix + semlit.pred[1:]
            args = []
            for arg in semlit.args:
                if arg[0] == '?':
                    args.append('?' + prefix + arg[1:])
                else:
                    args.append(arg)
            semlit.args = args

    def updateNode(self, prefix, node):
        if node.ntype == 'anchor': 
            if node.fs.has_key('prefix'):
                #node was the anchor of a tree involved in a previous rewriting
                node.fs = FeatStruct(self.updateFs(prefix, node.fs, 1, False))
            else:
                #projecting prefix for future lexicalisation
                node.fs = FeatStruct(self.updateFs(prefix, node.fs, 1, True))
        else:
            #update node names as well (cf coanchoring)
            if node.name is not None:
                newName = prefix + node.name
                if node.name in self.coanc.keys():
                    self.coanc[newName] = self.coanc[node.name]
                    del self.coanc[node.name]
                if node.name in self.names:
                    self.names[newName] = self.names[node.name]
                node.name = newName
            #update node fs
            node.fs = FeatStruct(self.updateFs(prefix, node.fs, 1, False))
        for x in node.children:
            self.updateNode(prefix, x)
 
    def updateIndexes(self, other, ignore):
        # subst nodes lists are merged
        updateDict(self.__subst, other.subst, None)
        # adj nodes lists are merged (but adjoined root node is ignored)
        updateDict(self.__adj, other.adj, ignore)
        # coanchor nodes lists are merged
        updateDict(self.__coanc, other.coanc, None)
        # named nodes lists are merged
        updateDict(self.__names, other.names, None)        
        # all anchor nodes are kept (for future anchoring)
        self.__anc.extend(other.anc)
        # numbers of adjunctions (per category) are added
        self.updateAdjoined(other.adjoined)

def updateDict(d1, d2, ignore):
    #d1 and d2 are two dictionaries whose values are lists
    #updateDict is used to merge d1 with info from d2
    for k,v in d2.items():
        vbis = list(filter(lambda x: x!= ignore, v)) if ignore is not None else v
        if k in d1.keys():
            d1[k].extend(vbis)
        else:
            d1[k] = vbis

            
########## JSON encoding #########
def entry2json(obj):
    res = dict()
    res['entry_id']  = obj.name
    res['family']    = obj.family
    res['trace']     = obj.trace
    res['semantics'] = sem2json(obj.semantics)
    res['iface']     = fs2json(obj.iface)
    res['tree']      = node2json(obj.tree)
    return res

def node2json(node):
    # Utility recursive function for JSON encoding
    res = dict()
    if hasattr(node, 'head'):
       res['head']= node.head
    if hasattr(node, 'name'):
        res['name']= node.name
    res['ntype']   = node.ntype
    res['fs']      = fs2json(node.fs)
    res['children']= list(map(node2json,node.children))
    return res

def fs2json(fs):
    # Utility recursive function for JSON encoding
    if fs is not None:
        if isinstance(fs, FeatStruct):
            res = {}
            for k,v in fs.items():
                res[k] = fs2json(v)
            return res
        elif isinstance(fs, Variable):
            return str(fs)
        else:
            return fs
    else:
        return None

def sem2json(sem):
    res = []
    for lit in sem:
        res.append(str(lit))
    return res


########## XML encoding #########
def trees2xml(entries, filename, progload):
    trees = etree.Element('grammar')
    progload.setTotal(len(entries))
    for e in range(len(entries)):
        entry = entries[e]
        if progload is not None:
            progload.value += 1
            if 'printProgressBar' in dir(progload):
                progload.printProgressBar()
        entry2xml(trees,entry)
    with open(filename, 'w') as outfile:
        print(etree.tostring(trees, encoding='UTF-8', pretty_print=True, xml_declaration=True, doctype='<!DOCTYPE grammar SYSTEM "xmg-tag.dtd,xml">').decode('utf-8'),file=outfile)
            
def entry2xml(motherDOM, obj):
    entry  = etree.SubElement(motherDOM,'entry',{'name': obj.name})
    family = etree.SubElement(entry,'family')
    family.text = obj.family
    trace  = etree.SubElement(entry,'trace')
    for t in obj.trace:
        classe = etree.SubElement(trace,'class')
        classe.text = t
    tree = etree.SubElement(entry,'tree', {'id': obj.name})
    node2xml(tree, obj.tree)
    semantics = etree.SubElement(entry,'semantics')            
    sem2xml(semantics, obj.semantics)
    interface = etree.SubElement(entry,'interface')                
    fs2xml(interface, obj.iface)

def node2xml(motherDOM, node):
    nodeDOM = etree.SubElement(motherDOM, 'node', {'type':node.ntype})
    if hasattr(node, 'name') and node.name is not None:
        nodeDOM.set('name', node.name)
    nargDOM = etree.SubElement(nodeDOM, 'narg')
    if node.fs is not None:
        fs2xml(nargDOM, node.fs)
    for n in node.children:
        node2xml(nodeDOM, n)

def fs2xml(motherDOM, fs):
        if isinstance(fs, FeatStruct):
            fsDOM = etree.SubElement(motherDOM, 'fs')
            res = {}
            for k,v in fs.items():
                valDOM = etree.SubElement(fsDOM, 'f', {'name':k})
                fs2xml(valDOM, v)
        elif isinstance(fs, Variable):
            etree.SubElement(motherDOM, 'sym', {'value':str(fs)})
        else:
            etree.SubElement(motherDOM, 'sym', {'varname':str(fs)})

def sem2xml(motherDOM, sem):
    for lit in sem:
        litDOM = etree.SubElement(motherDOM, 'literal')
        if lit.neg:
            litDOM.set('negated', 'yes')
        else:
            litDOM.set('negated', 'no')
        labDOM = etree.SubElement(litDOM, 'label')
        varval2xml(labDOM, lit.label)
        predDOM = etree.SubElement(litDOM, 'predicate')
        varval2xml(predDOM, lit.pred)
        for arg in lit.args:
            argDOM = etree.SubElement(litDOM, 'arg')
            varval2xml(argDOM, arg)
        
def varval2xml(motherDOM, x):
    if isinstance(x, Variable):
        etree.SubElement(motherDOM, 'sym', {'value':str(x)})
    else:
        etree.SubElement(motherDOM, 'sym', {'varname':str(x)})
    

########## TREE REWRITING #########
def applyBindings(node, bindings):
    # recursively apply bindings (unification percolation)
    if node.fs is None:
        return True
    elif node.fs.substitute_bindings(bindings) is not None:
        node.fs = node.fs.substitute_bindings(bindings)
        return all([applyBindings(x, bindings) for x in node.children])
    else:
        return False
    
def substitute(e1, e2, cat, index, verbose):
    """\
    e1 and e2 are two entries (tree + iface + sem + trace + family).
    cat,index is the postion in the subst list (node where to subst)
    """
    if e1 is None and e2 is None:
        return None
    elif e1 is None:
        return e2
    elif e2 is None:
        return e1
    else:
        #print(e1)
        e1bis = deepcopy(e1)
        if e1bis.isInitial:
            # initial tree instanciation comes with variable renaming
            e1bis.updateVars(nextint())
        if verbose:
            print(e1bis)
        #print(e2)
        e2bis = deepcopy(e2)
        if e2bis.isInitial:
            # initial tree instanciation comes with variable renaming    
            e2bis.updateVars(nextint())
        if verbose:
            print(e2bis)
        snode = e1bis.subst[cat].pop(index)
        rnode = e2bis.tree
        fst1  = snode.fs['top']
        fst2  = rnode.fs['top']
        bindings = {}
        res   = fst1.unify(fst2, bindings, trace=verbose)
        #print('fst1: '+str(fst1))
        #print('fst2: '+str(fst2))
        if res is not None and applyBindings(e1bis.tree, bindings) and applyBindings(rnode, bindings):
            e1bis.isInitial= False
            if verbose:
                print('Bindings: ' + str(bindings))
            snode.fs['top']= res
            snode.fs['bot']= rnode.fs['bot']
            # we substitute the substitution node with the corresponding root node
            # (the feature structures have already been unified)
            snode.children = rnode.children
            snode.ntype    = rnode.ntype
            # snode is now adjoinable
            if cat in e1bis.adj:
                e1bis.adj[cat].append(snode)
            else:
                e1bis.adj[cat] = [snode]
            # we update the other pieces of information (iface, trace, semantics, etc)
            e1bis.iface = e1bis.iface.unify(e2bis.iface, bindings)
            e1bis.trace.extend(e2bis.trace)            
            e1bis.semantics.extend(e2bis.semantics)
            e1bis.updateIndexes(e2bis, rnode) #direct access to nodes according to their type
            #print('remaining subst nodes :' + str(len(e1bis.subst[cat])))
            #we equip the derived tree with its bindings (for later)
            e1bis.bindings = bindings
            return e1bis
        else:
            return None
    

def adjoin(e1, e2, cat, index, verbose):
    """\
    e1 and e2 are two entries (tree + iface + sem + trace + family).
    cat,index is the postion in the adj list (node where to adjoin)
    """
    if e1 is None and e2 is None:
        return None
    elif e1 is None:
        return e2
    elif e2 is None:
        return e1
    else:
        #print(e1)
        e1bis = deepcopy(e1)
        if e1bis.isInitial:
            # initial tree instanciation comes with variable renaming    
            e1bis.updateVars(nextint())
        if verbose:
            print(e1bis)
        #print(e2)
        e2bis = deepcopy(e2)
        if e2bis.isInitial:
            # initial tree instanciation comes with variable renaming    
            e2bis.updateVars(nextint())
        if verbose:
            print(e2bis)
        anode = (e1bis.adj[cat])[index] # not poped cause anode should still be adjoinable
        # To print node memory address (for debug):        
        #print(cat + ' ' + str(id(anode)))
        rnode = e2bis.tree
        fnode = e2bis.foot[0]
        fst1  = anode.fs['top']
        fsb1  = anode.fs['bot']
        fst2  = rnode.fs['top']
        fsb2  = fnode.fs['bot']
        bindings = {**e1bis.bindings, **e2bis.bindings} if e1bis.bindings is not None and e2bis.bindings is not None else {} #dict merge
        res1  = fst1.unify(fst2, bindings, trace=verbose)
        res2  = fsb1.unify(fsb2, bindings, trace=verbose)
        if res1 is not None and res2 is not None and applyBindings(e1bis.tree, bindings) and applyBindings(e2bis.tree, bindings):
            e1bis.isInitial= False
            if cat in e1bis.adjoined:
                e1bis.adjoined[cat] += 1
            else:
                e1bis.adjoined[cat]  = 1
            if verbose:
                print('Bindings: ' + str(bindings))
            anode.fs['top'] = res1
            anode.fs['bot'] = rnode.fs['bot'] # adjunction site's bot is now former root's bot
            fnode.fs['bot'] = res2
            #we adjoin e2 in e1 at anode
            tmp_nodes      = anode.children
            anode.children = rnode.children
            fnode.children = tmp_nodes
            anode.ntype    = rnode.ntype
            fnode.ntype    = rnode.ntype
            # fnode's name is now the former adjunction site's name (if any)
            e1bis.names[anode.name] = [fnode]
            fnode.name = anode.name
            # fnode is now adjoinable
            if cat in e1bis.adj:
                e1bis.adj[cat].append(fnode)
            else:
                e1bis.adj[cat] = [fnode]
            #we update the other pieces of information (iface, trace, semantics, etc)
            e1bis.iface    = e1bis.iface.unify(e2bis.iface, bindings)
            e1bis.trace.extend(e2bis.trace)
            e1bis.semantics.extend(e2bis.semantics)            
            e1bis.updateIndexes(e2bis, rnode) # rnode is ignored in the list of adjoinable nodes
            #we equip the derived tree with its bindings (for later)
            e1bis.bindings = bindings
            return e1bis
        else:
            return None

def topbotunify(node, bindings, verbose):
    fst1 = node.fs['top']
    fst2 = node.fs['bot']    
    res  = fst1.unify(fst2, bindings, trace=verbose)
    node.fs = node.fs.substitute_bindings(bindings)            
    return res is not None and all([topbotunify(x, bindings, verbose) for x in node.children])
