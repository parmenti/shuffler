# -*- coding: utf-8 -*-
"""\
This module contains the utility classes and functions
Author: Yannick Parmentier
Date: 2017/10/17
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import threading

class IdGenerator(object):
    def __init__(self):
        self.cur_id = 0
        self.lock = threading.Lock()
    def next_id(self):
        with self.lock:
            result = self.cur_id
            self.cur_id += 1
        return result

idgen = IdGenerator()
    
def nextint():
    global idgen
    return idgen.next_id()

class VarNameGenerator(object):

    def __init__(self):
        self.newNames = []
        for i in range(ord('A'), ord('Z')):
            self.newNames.append(chr(i))
        self.availableI = 0 #index of the next letter to be used
        self.complete   = 0 #how many times did we use the full set of letters
        self.var2name   = {}
        self.name2var   = {}

    def getName(self, var):
        if var in self.var2name:
            return self.var2name[var]
        else:
            name = self.newNames[self.availableI] + str(self.complete)
            nextI= self.availableI + 1
            if nextI >= len(self.newNames):
                self.complete += 1
            self.availableI = nextI % len(self.newNames)
            self.var2name[var] = name
            self.name2var[name]= var
            return name
